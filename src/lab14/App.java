package lab14;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class App {

	private JFrame frame;
	private JTextField aTextField;
	private JTextField bTextField;
	private JTextField cTextField;
	private JTextField x1textField;
	private JTextField x2textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App window = new App();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 483, 185);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		frame.getAccessibleContext().setAccessibleName("Ramka");
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Tr\u00F3jmian kwadratowy", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel.setBounds(16, 16, 244, 43);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		aTextField = new JTextField();
		aTextField.setBounds(6, 16, 55, 20);
		panel.add(aTextField);
		aTextField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("x\u00B2 +");
		lblNewLabel.setBounds(71, 19, 24, 14);
		panel.add(lblNewLabel);
		
		bTextField = new JTextField();
		bTextField.setBounds(100, 16, 55, 20);
		panel.add(bTextField);
		bTextField.setColumns(10);
		
		JLabel lblX = new JLabel("x +");
		lblX.setBounds(160, 19, 24, 14);
		panel.add(lblX);
		
		cTextField = new JTextField();
		cTextField.setBounds(183, 16, 55, 20);
		panel.add(cTextField);
		cTextField.setColumns(10);
		
		panel.getAccessibleContext().setAccessibleName("Pierwiastki tr�jmianu kwadratowego");
		
		JButton btnNewButton = new JButton("Wyznacz pierwiastki");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String aString = aTextField.getText();
				if(aString.equals("")) aString = "0";
				aString.replace(',', '.');

				String bString = bTextField.getText();
				if(bString.equals("")) bString = "0";
				bString.replace(',', '.');

				String cString = cTextField.getText();
				if(cString.equals("")) cString = "0";
				cString.replace(',', '.');
				
				try {
					BigDecimal a = new BigDecimal(aString);
					BigDecimal b = new BigDecimal(bString);
					BigDecimal c = new BigDecimal(cString);
					
					QuadraticEquation qe = new QuadraticEquation(a, b, c);
					
					ArrayList<BigDecimal> roots = qe.getRoots();
					
					switch(roots.size()) {
						case 2:
							x1textField.setEnabled(true);
							x2textField.setEnabled(true);
							
							x1textField.setText(roots.get(0).toString());
							x2textField.setText(roots.get(1).toString());
							break;
						case 1:
							x1textField.setEnabled(true);
							x2textField.setEnabled(false);
							
							x1textField.setText(roots.get(0).toString());
							x2textField.setText("");
							break;
						case 0:
							x1textField.setEnabled(false);
							x2textField.setEnabled(false);
							
							x1textField.setText("");
							x2textField.setText("");
							
							JOptionPane.showMessageDialog(null, "Podany tr�jmian kwadratowy nie ma pierwiastk�w.", "Uwaga", JOptionPane.INFORMATION_MESSAGE);
					}
				}
				catch(NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage(), "B��d", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(270, 31, 197, 23);
		frame.getContentPane().add(btnNewButton);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "1. pierwiastek", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_1.setBounds(35, 92, 98, 43);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		x1textField = new JTextField();
		x1textField.setBounds(6, 16, 86, 20);
		panel_1.add(x1textField);
		x1textField.setEditable(false);
		x1textField.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "2. pierwiastek", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_2.setBounds(146, 91, 98, 43);
		frame.getContentPane().add(panel_2);
		
		x2textField = new JTextField();
		x2textField.setEditable(false);
		x2textField.setColumns(10);
		x2textField.setBounds(6, 16, 86, 20);
		panel_2.add(x2textField);
	}
}
